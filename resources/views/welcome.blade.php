<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>ДеньгиСейчас: Займы быстро</title>
    <meta data-n-head="ssr" charset="utf-8">
    <meta data-n-head="ssr" name="viewport" content="width=device-width, initial-scale=1 shrink-to-fit=no">
    <meta data-n-head="ssr" data-hid="description" name="description" content="">
    <link data-n-head="ssr" data-hid="icon" rel="icon" type="image/x-icon" href="/favicon.svg?u24">

    <meta name="verify-admitad" content="682c2d4b87" />

    <meta name="keywords"
          content="займы, быстро, занять деньги сейчас, до 10 минут, плохая кредитная история, деньги на карту, быстрозайм, займы всем, микрозаймы">

    <meta name="description"
          content="ДеньгиСейчас | Займы всем | Оформление без посещения банка | Деньги мгновенно поступают на имеющуюся у вас банковскую карту | Минимальное количество документов | Полученные деньги можно снять в банкомате или использовать для безналичных покупок | Возможность получить деньги даже с плохой кредитной историей">

    <link href="{{asset('css/app.css')}}" rel="stylesheet" type="text/css">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(80339743, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/80339743" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</head>
<body>
<div id="__nuxt">
    <div id="__layout">
        <div class="b-overlay-wrap position-relative" data-v-ebd6815c="">
            <div class="app-landings" data-v-ebd6815c="">
                <div class="box-shadow px-0 container-fluid" data-v-452031dd="">
                    <nav class="container-lg" data-v-452031dd="">
                        <div class="row" data-v-452031dd="">
                            <div class="col" data-v-452031dd="">
                                <div class="nav d-flex justify-content-between align-items-center" data-v-452031dd="">
                                    <div class="nav-logo" data-v-452031dd="">ДеньгиСейчас.рф</div>
                                    <div class="nav-region" data-v-452031dd="">
                                        <div class="dropdown b-dropdown position-static btn-group" data-v-452031dd=""
                                             id="__BVID__13"><!---->
                                            <button aria-haspopup="true" aria-expanded="false" type="button"
                                                    class="btn dropdown-toggle btn-link btn-lg m-0 p-0 dropdown-toggle-no-caret"
                                                    id="__BVID__13__BV_toggle_"><span class="text-md"
                                                                                      data-v-452031dd="">Новосибирск</span>
                                            </button>
                                            <ul role="menu" tabindex="-1"
                                                class="dropdown-menu nav-city-menu dropdown-menu-right"
                                                aria-labelledby="__BVID__13__BV_toggle_" style="">
                                                <li role="presentation" data-v-452031dd="">
                                                    <form tabindex="-1" class="b-dropdown-form" data-v-452031dd="">
                                                        <div role="group" class="form-group" data-v-452031dd=""
                                                             id="__BVID__14"><label for="dropdown-form-city"
                                                                                    class="d-block"
                                                                                    id="__BVID__14__BV_label_">Выбор
                                                                города</label>
                                                            <div class="bv-no-focus-ring"><input id="dropdown-form-city"
                                                                                                 type="text"
                                                                                                 placeholder="Начните вводить название"
                                                                                                 value=""
                                                                                                 class="form-control form-control-md"
                                                                                                 data-v-452031dd="">
                                                                <!----><!----><!----></div>
                                                        </div>
                                                    </form>
                                                </li>
                                                <li role="presentation" data-v-452031dd="">
                                                    <hr role="separator" aria-orientation="horizontal"
                                                        class="dropdown-divider" data-v-452031dd="">
                                                </li>
                                                <li class="b-overlay-wrap position-relative" data-v-452031dd="">
                                                    <div class="nav-city-menu-list-wrapper" data-v-452031dd="">
                                                        <ul class="nav-city-menu-list" data-v-452031dd="">
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Москва
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Санкт-Петербург
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Нижний-Новгород
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Самара
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Новосибирск
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Екатеринбург
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Казань
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Челябинск
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Омск
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Ростов-на-Дону
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Уфа
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Красноярск
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Пермь
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Воронеж
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Волгоград
                                                            </li>
                                                            <li class="nav-city-menu-list-item text-sm"
                                                                data-v-452031dd="">Краснодар
                                                            </li>
                                                        </ul>
                                                    </div><!----></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <main class="container-lg">
                    <div class="row mt-1">
                        <div class="col-md-7 col-12">
                            <div class="header-content"><h1 class="header-title" style="--text-align:left; --margin:0;">
                                    Лучшие предложения для Вас</h1>
                                <div class="text-left header-text">
                                    <ul>
                                        <li>Деньги на карте через 10 минут&nbsp;</li>
                                        <li>С плохой кредитной историей</li>
                                        <li>Без отказов</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="justify-content-end header-picture"><img
                                    src="https://unicom24.ru/media/open/5/e/b5/eb581439d83ef3b326afcccb85c71417.jpg/resizer/290/0"
                                    alt="" class="img-fluid"></div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="desktop-card">
                                <div onclick="location.href='https://ad.admitad.com/g/3q5bfxdfwi682c2d4b877e790fd18e';" tabindex="1" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137="">
                                    <div class="offer-best" data-v-0db6b137="">
                                        <div class="offer-best-title" data-v-0db6b137="">
                                            <svg viewBox="0 0 16 16" width="1em" height="1em" focusable="false"
                                                 role="img" aria-label="lightning fill"
                                                 xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                 class="bi-lightning-fill d-none d-sm-inline-block b-icon bi"
                                                 data-v-0db6b137="">
                                                <g data-v-0db6b137="">
                                                    <path fill-rule="evenodd"
                                                          d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                                </g>
                                            </svg>
                                            <span class="text-accent-xxs" data-v-0db6b137="">Лучшее предложение</span>
                                        </div>
                                    </div>
                                    <div class="offer-block offer-block-list-popular" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/1/0/19/019750f66649eb510a41552beffa28c3.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Zaymigo</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 день
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="2" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137="">
                                    <div class="offer-best" data-v-0db6b137="">
                                        <div class="offer-best-title" data-v-0db6b137="">
                                            <svg viewBox="0 0 16 16" width="1em" height="1em" focusable="false"
                                                 role="img" aria-label="lightning fill"
                                                 xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                 class="bi-lightning-fill d-none d-sm-inline-block b-icon bi"
                                                 data-v-0db6b137="">
                                                <g data-v-0db6b137="">
                                                    <path fill-rule="evenodd"
                                                          d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                                </g>
                                            </svg>
                                            <span class="text-accent-xxs" data-v-0db6b137="">Лучшее предложение</span>
                                        </div>
                                    </div>
                                    <div class="offer-block offer-block-list-popular" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/1/5/8c/58cc52abfe579773fdf5c7219cdb3fe2.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">MoneyMan</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 5 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="3" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137="">
                                    <div class="offer-best" data-v-0db6b137="">
                                        <div class="offer-best-title" data-v-0db6b137="">
                                            <svg viewBox="0 0 16 16" width="1em" height="1em" focusable="false"
                                                 role="img" aria-label="lightning fill"
                                                 xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                 class="bi-lightning-fill d-none d-sm-inline-block b-icon bi"
                                                 data-v-0db6b137="">
                                                <g data-v-0db6b137="">
                                                    <path fill-rule="evenodd"
                                                          d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                                </g>
                                            </svg>
                                            <span class="text-accent-xxs" data-v-0db6b137="">Лучшее предложение</span>
                                        </div>
                                    </div>
                                    <div class="offer-block offer-block-list-popular" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/4/2/30/230c24dc2ac047ba10b04ec47d171549.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Belka Credit</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="4" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137="">
                                    <div class="offer-best" data-v-0db6b137="">
                                        <div class="offer-best-title" data-v-0db6b137="">
                                            <svg viewBox="0 0 16 16" width="1em" height="1em" focusable="false"
                                                 role="img" aria-label="lightning fill"
                                                 xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                                                 class="bi-lightning-fill d-none d-sm-inline-block b-icon bi"
                                                 data-v-0db6b137="">
                                                <g data-v-0db6b137="">
                                                    <path fill-rule="evenodd"
                                                          d="M11.251.068a.5.5 0 0 1 .227.58L9.677 6.5H13a.5.5 0 0 1 .364.843l-8 8.5a.5.5 0 0 1-.842-.49L6.323 9.5H3a.5.5 0 0 1-.364-.843l8-8.5a.5.5 0 0 1 .615-.09z"></path>
                                                </g>
                                            </svg>
                                            <span class="text-accent-xxs" data-v-0db6b137="">Лучшее предложение</span>
                                        </div>
                                    </div>
                                    <div class="offer-block offer-block-list-popular" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/0/a/5b/a5b2da584c673380fa467965cd7b0385.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">До Зарплаты - На год</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 100 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 0,5 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 60 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="5" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/0/a/5b/a5b2da584c673380fa467965cd7b0385.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">До Зарплаты - под 0%</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 20 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="6" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/2/d/f1/df13d9af0e2d1f55fddbe6821ed70ffb.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Cash-U Finance</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 5 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="7" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/3/b/2b/b2b3f88bbb1a7324bb19cfe70541ebb4.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Займер</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="8" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/4/c/f4/cf40fdf3e47cc83e9e6108eaa85bbc63.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Турбозайм</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 50 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 0,5 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="9" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/5/4/46/446add8ad4f0c291d8dbb410f5b21c4f.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Max.Credit</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 5 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="10" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/1/f/4e/f4eeaf372b1c47a6d3d2d0e89a150107.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Credit7</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="11" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/4/0/72/0724857313094da30365545630bc3318.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Kredito24</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 15 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 16 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="12" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/2/d/8c/d8c95bf9bb5befd717b6c85275b25077.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">ФИНТЕРРА</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 день
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="13" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/2/5/75/575be7e6261467e79484f831273190d1.jpg/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">GreenMoney</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 35 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 3 дня
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="14" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/2/d/00/d006f7778c1082cf83a1fe12eb794c4a.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Джой Мани "Мастер"</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 15 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 10 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="15" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/1/4/ea/4eae51b67fa53c09769b19f9d6d4faaa.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Ezaem</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 5 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="16" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/6/5/b5/5b599f5ee55f99a82ac3b687b33e628b.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">FastMoney</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="17" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/4/e/b9/eb901f819cc436fe80622598e9f0354e.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">ДоброЗайм "Своим +"</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 100 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 0,91 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 62 дня
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="18" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/2/5/aa/5aabe2d40648720b31e457e5fd5e5681.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">Pay P.S.</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 100 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 5 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="19" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/3/c/74/c74ac054566fe45104a4fdc05d892b01.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">VIVUS.RU – беспроцентный
                                                период 7 дней!
                                            </div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 100 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 1 день
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div tabindex="20" role="button"
                                     class="offer-view d-flex flex-column justify-content-end mob-card"
                                     data-v-0db6b137=""><!---->
                                    <div class="offer-block" data-v-0db6b137="">
                                        <div class="offer-block-brand qiwi" data-v-0db6b137="">
                                            <div class="offer-block-logo d-flex" data-v-0db6b137=""><img
                                                    src="https://unicom24.ru/media/open/4/a/02/a02307dd2024d1c7863c06ff11c7c518.png/resizer/100/0"
                                                    alt="" data-v-0db6b137="" class=""></div>
                                            <div class="offer-block-title" data-v-0db6b137="">еКапуста</div>
                                        </div>
                                        <div class="offer-block-info" data-v-0db6b137="">
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs text-bold"
                                                         data-v-0db6b137="">Сумма
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs text-bold"
                                                     data-v-0db6b137="">до 30 000 ₽
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Ставка
                                                    </div> <!----></div>
                                                <div
                                                    class="offer-block-info-value text-microcards-xs text-lite-bold text-blue"
                                                    data-v-0db6b137="">от 0 %
                                                </div>
                                            </div>
                                            <div class="offer-block-info-item" data-v-0db6b137="">
                                                <div class="d-flex" data-v-0db6b137="">
                                                    <div class="offer-block-info-title text-microcards-xs"
                                                         data-v-0db6b137="">Срок
                                                    </div> <!----></div>
                                                <div class="offer-block-info-value text-microcards-xs"
                                                     data-v-0db6b137="">от 7 дней
                                                </div>
                                            </div>
                                        </div>
                                        <div class="offer-block-button" data-v-0db6b137="">
                                            <button type="button" class="btn btn-secondary" data-v-0db6b137="">Получить
                                                деньги
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <div class="bg-background container-fluid">
                    <footer class="container-lg py-4">
                        <div class="row">
                            <div class="col-md-10 col-lg-8 col-12"><h4><span style="font-size:12px">Преимущества микрозаймов через Интернет:</span>
                                </h4>

                                <ul>
                                    <li><span style="font-size:12px">Оформление без посещения банка (и даже не выходя из дома)</span>
                                    </li>
                                    <li><span style="font-size:12px">Деньги мгновенно поступают на имеющуюся у вас банковскую карту</span>
                                    </li>
                                    <li><span style="font-size:12px">Минимальное количество документов (чаще всего необходим только паспорт)</span>
                                    </li>
                                    <li><span style="font-size:12px">Полученные деньги можно снять в банкомате или использовать для безналичных покупок</span>
                                    </li>
                                    <li><span style="font-size:12px">Возможность получить деньги даже с плохой кредитной историей.</span>
                                    </li>
                                </ul>

                                <p>Не является микрофинансовой или кредитной организацией, не выдает займы и не
                                    привлекает денежных средств. Информация, размещенная на сайте, носит исключительно
                                    ознакомительный характер. Все условия и решения, касающиеся получения займов или
                                    кредитов, принимаются непосредственно компаниями, предоставляющими данные
                                    услуги.<br>
                                    Мы используем файлы cookie для того, чтобы предоставить пользователям больше
                                    возможностей при посещении сайта.</p></div>
                        </div>
                    </footer>
                </div>
            </div><!----></div>
    </div>
</div>
</body>
</html>
